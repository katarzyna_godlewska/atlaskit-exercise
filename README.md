**Comments to the task**

- In addition to the required Atlaskit components, the Bootstrap package was used to prepare the page layout. Thanks to this, it was possible to quickly prepare a responsive website.
- Page has required sections as: header with logo, simple menu on the left side, main section (for form and profile info) and footer.
- Vuelidation library with custom rules (placed in separate file) was used to validate form. All inputs in form are required. Additionaly, email input value must be valid email address, birthday input value must be valid date and avatar input allowed file extensions are ".jpg", ".jpeg", ".png". Uploaded file max size is 2MB. The length of text values is also checked.
- As submitted data should be session persistent, sessionStorage was used to store data. Both methods used to save and get user data return promise. For now in the getUserInfo method it is completely unnecessary, but in the future it would be easier to connect to the API. Nothing in the components would have to be changed to make asynchronous calls, only changes in this two methods would be needed.

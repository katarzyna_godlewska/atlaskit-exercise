import Vue from 'vue'
import App from './App.vue'
import router from './router'

// Vue.config.productionTip = false

import Vuelidate from 'vuelidate'
Vue.use(Vuelidate)

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

import '@spartez/vue-atlaskit/dist/bundle.css'

import '@/assets/styles/app.scss'

new Vue({
    router,
    render: (h) => h(App),
}).$mount('#app')

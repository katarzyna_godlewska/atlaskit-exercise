const getUserInfo = () => {
    return new Promise((resolve) => {
        resolve({
            success: true,
            user: JSON.parse(sessionStorage.getItem('user'))
        })
    })
}

const saveUserInfo = (userInfo) => {
    return new Promise((resolve, reject) => {
        const reader = new FileReader()

        reader.onload = () => {
            sessionStorage.setItem('user', JSON.stringify({
                ...userInfo,
                avatar: reader.result
            }))

            resolve({
                success: true
            })
        }

        reader.onerror = () => {
            reject({
                success: false
            })
        }

        reader.readAsDataURL(userInfo.avatar)
    })
}

export {getUserInfo, saveUserInfo}
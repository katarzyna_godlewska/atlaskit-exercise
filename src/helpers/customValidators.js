import moment from 'moment'

// 2097152 B -> 2 MB
const fileSizeValidator = value => !value ? true : (value.size < 2097152)

const phoneValidator = value => !value ? true : /^([0-9]){9}$/i.test(value)

const dateValidator = value => !value ? true : moment(value).isValid()

export {fileSizeValidator, phoneValidator, dateValidator}